/**
 * 
 */
package cc.renken.validate;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.ElementType.TYPE_USE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Repeatable;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

@Target({ FIELD, METHOD, PARAMETER, ANNOTATION_TYPE, TYPE_USE })
@Retention(RUNTIME)
@Constraint(validatedBy = { TypeReferenceStringValidator.class, TypeReferenceClassValidator.class, TypeReferenceObjectValidator.class })
@Documented
@Repeatable(TypeReference.List.class)
public @interface TypeReference
{

    String message() default "Given class reference does not match constraints.";

    Class<?>[] groups() default { };

    Class<? extends Payload>[] payload() default { };

    /**
     * Specifies a super type (super class or interface) that the actual value must inherit from.
     * May be empty - then this check is skipped.
     * @return Optional, a super type that the verified value must inherit from.
     */
    Class<?> value() default Void.class;
    
    boolean mayBeNull() default false;

    @Target({ FIELD, METHOD, PARAMETER, ANNOTATION_TYPE })
    @Retention(RUNTIME)
    @Documented
    @interface List {
    	TypeReference[] value();
    }
}
