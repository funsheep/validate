/**
 * 
 */
package cc.renken.validate;

/**
 * @author renkenh
 *
 */
public class TypeReferenceObjectValidator extends ATypeReferenceValidator<Object>
{

	@Override
	protected Class<?> toClass(Object value)
	{
		return value.getClass();
	}

}
