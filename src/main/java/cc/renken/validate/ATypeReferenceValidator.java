/**
 * 
 */
package cc.renken.validate;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * @author renkenh
 *
 */
abstract class ATypeReferenceValidator<PARAM_TYPE> implements ConstraintValidator<TypeReference, PARAM_TYPE>
{

	private TypeReference reference;

	@Override
	public final void initialize(TypeReference constraintAnnotation)
	{
		this.reference = constraintAnnotation;
	}

	@Override
	public final boolean isValid(PARAM_TYPE value, ConstraintValidatorContext context)
	{
		if (this.reference == null || this.reference.value().equals(Void.class))
			return true;
		if (value == null)
			return this.reference.mayBeNull();
		
		Class<?> clazz = this.toClass(value);
		if (clazz == null)
			return false;
		return this.reference.value().isAssignableFrom(clazz);
	}

	protected abstract Class<?> toClass(PARAM_TYPE value);
}
