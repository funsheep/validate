/**
 * 
 */
package cc.renken.validate;

/**
 * @author renkenh
 *
 */
public class TypeReferenceStringValidator extends ATypeReferenceValidator<String>
{

	@Override
	protected Class<?> toClass(String value)
	{
		try
		{
			return Class.forName(value);
		}
		catch (ClassNotFoundException e)
		{
			return null;
		}
	}

}
