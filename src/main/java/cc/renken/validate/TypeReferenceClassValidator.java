/**
 * 
 */
package cc.renken.validate;

/**
 * @author renkenh
 *
 */
public class TypeReferenceClassValidator extends ATypeReferenceValidator<Class<?>>
{

	@Override
	protected Class<?> toClass(Class<?> value)
	{
		return value;
	}

}
